FROM node:13.12.0-alpine
WORKDIR /oneurl
RUN apk update
RUN apk add git bash bash-completion
COPY package.json .
COPY yarn.lock .
RUN yarn
ENV PS1="dev> "
CMD ["yarn", "test"]