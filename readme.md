oneurl
======

## Development

After `git clone`ing this repository, with docker installed, run
`./dev [command]`, where command runs within the development 
container, where all dependencies needed to run it shall be automatically
loaded for you.

Run `./dev` by itself to go to a bash prompt within the development
container.

## Experimental

Running `./coder` brings up visual studio code environment that can
be navigated to within your browser (see the output for the URL)

You can use the embedded terminal in your browser to run commands within
the container.

If at any time we wish to develop against a remote environment, possibly
comprising several resources (eg. kubernetes pods in a namspace just
for the development environment), this tooling may simplify that, as well
as enable us to use low powered devices to perform software programming
rather than expensive laptops.

If we wish to run a local kubernetes cluster on our device, so that the
environment offline matches closely to the environment online and production
itself, we can do so in a way that codifies how the IDE would interact with
such a system.

In this way, all developers can access a ready-to-go, codified, standardized,
evolving development environment, that they are free to enhance as well
as customize over time.

A service might accept only the git repository URL and set up an entire
running dev stack in the cloud with a full screen browser-based IDE like
VS-Code, IntelliJ IDEA, and others.