import { O, F, L } from "ts-toolbelt"

type CaseClass<Discriminator extends string> = { type: Discriminator }

type CaseMatch<Discriminator extends string> = 
    (caseClass:CaseClass<Discriminator | string>) => caseClass is CaseClass<Discriminator>

type When = 
    <Discriminator extends string>(discriminator:Discriminator) =>
    CaseMatch<Discriminator>

const when:When =
    <Discriminator extends string>(discriminator:Discriminator):CaseMatch<Discriminator> =>
    (caseClass):caseClass is CaseClass<Discriminator> =>
    caseClass.type === discriminator

const caseObject =
    <Discriminator extends string>
    (type:Discriminator):[CaseClass<Discriminator>, CaseMatch<Discriminator>] => 
    {
        const caseObject = { type }
        const valueDestructor = when(type)
        return [caseObject, valueDestructor]
    }

type CaseClassHelpers<Discriminator extends string, P extends L.List<any>, R> = [
    F.Function<P, R & CaseClass<Discriminator>>, 
    CaseMatch<Discriminator>, 
    R & CaseClass<Discriminator>
]

const caseClass = 
    <Discriminator extends string>
    (type:Discriminator) => 
    <P extends L.List<any>, R>
    (constructorFunction:F.Function<P, R>):CaseClassHelpers<Discriminator, P, R> =>
    {
        const valueConstructor = 
            (...args:P) =>
            ({
                ...constructorFunction(...args),
                type
            })
        const valueDestructor = when(type)
        return [valueConstructor, valueDestructor, null as any as R & CaseClass<Discriminator>]
    }

////////////////////////////

const [passed, Passed] = caseObject("Passed")
type Passed = typeof passed

const [failed, Failed, FailedType] = caseClass("Failed")((error:Error) => ({error}))
type Failed = typeof FailedType

type TestOutcome = Passed | Failed

//todo: case matching

type Test = () => TestOutcome
type Tests = Array<Test>
type JestTests = (...tests:Tests) => void

const jestify:JestTests = 
    (...tests) =>
    describe("tests", () => {
        tests.map((test, index) => it(`passes test ${index}`, () => {
            const result = test()
            if(Failed(result))
                throw new Error(`Failed test ${index} with error ${result.error}`)
        }))
    })

type JestItBlock = () => any
type JestItBlocks = JestItBlock[]
type Expectation = (expression:JestItBlock) => Test
const expectation:Expectation = 
    expression => 
    () =>
    {
        try {
            expression()
            return passed
        }
        catch(e) {
            return failed(e)
        }
    }

const failWithMessage = (message:string) => failed(new Error(message))

const expectations = 
    (...expectations:JestItBlocks) =>
    expectations.map(expectation)

/////////////////////////////

type LackOfValue = {
    map: () => LackOfValue,
    Then: () => LackOfValue,
    mapOrDefault: () => <D>(d:D) => D
    flatMap: () => LackOfValue
}

const lackOfValue:LackOfValue = {
    map: () => lackOfValue,
    Then: () => lackOfValue,
    mapOrDefault: () => d => d,
    flatMap: () => lackOfValue
}

type ActualValue<T> = { 
    value: T,
    map: <R>(f:F.Function<[T], R>) => ActualValue<R>,
    Then: <R>(r:R) => ActualValue<R>,
    mapOrDefault: <R, D>(f: F.Function<[T], R>) => (d:D) => R | D
    flatMap: <R>(f:F.Function<[T], Possible<R>>) => Possible<R>
}

const actualValue = 
    <T>(value:T):ActualValue<T> =>
    ({
        value,
        map: f => actualValue(f(value)),
        Then: actualValue,
        mapOrDefault: f => d => f(value),
        flatMap: f => f(value)
    })

type Possible<T> = LackOfValue | ActualValue<T>

/////////////////////////////

type Url = {
    path:string
}

type Request = { url: Url }
type Producer<T> = () => T
type BrowserResponse = {}
const browserResponse:BrowserResponse = {}

/////////////////////////////

//OO Stringables

type PossiblyStringableUrl<T> = {} extends T ? StringableUrl : NonStringableUrl<T>
type SharedUrl<T extends {}> = {
    match: (url:Url) => Possible<T>
    addPath: (path:string) => PossiblyStringableUrl<T>
    addPathParam: <P2 extends string>(p2:P2) => NonStringableUrl<Record<P2, string> & T>
}

type NonStringableUrl<T extends {}> = SharedUrl<T> & {
    unresolvedParams: O.Keys<T>[]
    setPathParam: <P2 extends keyof T>(p2:P2) => (value:string) => {} extends Omit<T, P2> 
        ? StringableUrl 
        : NonStringableUrl<Omit<T, P2>>
    addPath: (path:string) => NonStringableUrl<T>
}

type StringableUrl = SharedUrl<{}> & {
    toUrlString: () => string
}

const [ staticSegment, StaticSegment, StaticSegmentType] = caseClass("StaticSegment")((path:string) => ({path}))
type StaticSegment = typeof StaticSegmentType

const [ dynamicSegment, DynamicSegment, DynamicSegmentType] = caseClass("DynamicSegment")((pathParam:string) => ({pathParam}))
type DynamicSegment = typeof DynamicSegmentType

type Segment = StaticSegment | DynamicSegment
type Segments = Segment[]

const throwError = (message:string) => { throw new Error(message) }

type WithValue = <T>(t:T) => <R>(f:(t:T) => R) => R
const withValue:WithValue = t => f => f(t)

type SegmentAccumulator = {path: string, matches: StringDictionary }
const segmentReducer = 
    (possibleAccumulator:Possible<SegmentAccumulator>, segment:Segment) =>
    possibleAccumulator.flatMap(
        ({path, matches}) =>
        StaticSegment(segment) 
            ? path.indexOf(segment.path) == 0
                ? actualValue({ path: path.substring(segment.path.length), matches})
                : lackOfValue
            : withValue
                (path.substring(0, path.indexOf("/") == -1 ? undefined : path.indexOf("/")))
                (   
                    possiblyEmptyString => 
                    possiblyEmptyString == ""
                        ? lackOfValue
                        : actualValue({ 
                            path: path.substring(possiblyEmptyString.length), 
                            matches: {...matches, [segment.pathParam]: possiblyEmptyString}
                        })
                )
    )

const unsafePossiblyStringable = 
    (segments:Segments) => 
    ({
        addPath: (path:string) => unsafePossiblyStringable([...segments, staticSegment(path)]),
        toUrlString: () => segments.map(s => StaticSegment(s) ? s.path : throwError("cannot toUrlString, missing segments, should never see this")).join("/").replace(/\/+/g,"/"),
        get unresolvedParams() { return segments.filter(DynamicSegment) },
        match: (url:Url) => segments.reduce(
            segmentReducer,
            actualValue({ path: url.path, matches: {} })
        ).flatMap(m => m.path == "" ? actualValue(m.matches) : lackOfValue),
        addPathParam: (pathParam:string) => unsafePossiblyStringable([...segments, dynamicSegment(pathParam)]),
        setPathParam: (pathParam:string) => (value:string) => unsafePossiblyStringable(segments.map(s => DynamicSegment(s) ? s.pathParam == pathParam ? staticSegment(value) : s : s))
    })

const newStringable = 
    (startingPath:string):StringableUrl => 
    unsafePossiblyStringable([staticSegment(startingPath)]) as StringableUrl

type Dictionary<T> = { [key:string]: T }
type StringDictionary = Dictionary<String>

////////////////////////////////////

type Route = (r:Request) => Possible<Producer<BrowserResponse>>

declare const exampleUrl:StringableUrl
const exampleRoute:Route = 
    request =>
    exampleUrl
        .match(request.url)
        .map((j) => () => browserResponse)

//TODO: have a functional / pipe way to do this so need not start with object up front.
//TODO: when adding params, make it accept non-string type
//TODO: <P>partialMatch(prop:P) => Possible<{match:Record<P, string>, remainder:PossiblyStringableUrl<T - P>}>

const rootPath = "/"
const rootUrl = newStringable(rootPath)
const urlNeedingId = rootUrl.addPathParam("id")
const idWelcomeUrl = urlNeedingId.addPath("welcome")
const urlThatHasLiteralSectionWithSlashes = urlNeedingId.addPath("section/with/slashes")

const p = {
    "/": rootUrl,
    "/:id": urlNeedingId,
    "/:id/section/with/slashes": urlThatHasLiteralSectionWithSlashes,
    "/:id/welcome": idWelcomeUrl,
    "/:id/welcome/:id2/something": idWelcomeUrl.addPathParam("id2").addPath("something"),
    "/:id/welcome/something/:id2/somethingElse": 
        idWelcomeUrl.addPath("something").addPathParam("id2").addPath("somethingElse"),
    "welcome": newStringable("welcome"),

    //todo: querystrings (optional and required)
}

jestify(
    ...expectations(
        () => expect(p["/"].toUrlString()).toEqual(rootPath),
        () => expect(p["/:id"].match({path: "/tommy"}).mapOrDefault(({id}) => id)("default")).toEqual("tommy"),
        () => expect(p["/:id"].match({path: "/tommy/something"}).mapOrDefault(({id}) => id)("default")).toEqual("default"),
        () => expect(p["/:id"].setPathParam("id")("tommy").toUrlString()).toEqual("/tommy"),
        () => expect(p["/:id/section/with/slashes"].setPathParam("id")("tommy").toUrlString()).toEqual("/tommy/section/with/slashes"),
        () => expect(p["/:id/welcome/something/:id2/somethingElse"].setPathParam("id")("tommy").setPathParam("id2")("mike").toUrlString()).toEqual("/tommy/welcome/something/mike/somethingElse"),
        () => expect(p["/:id/welcome/something/:id2/somethingElse"].setPathParam("id")("/tommy//hi").setPathParam("id2")("mike").toUrlString()).toEqual("/tommy/hi/welcome/something/mike/somethingElse"),
        () => expect(p["welcome"].toUrlString()).toEqual("welcome"),
        
        //below should yield compile errors if uncommented
        // () => expect(p["/:id"].toUrlString()).toEqual(rootPath),
    )
)